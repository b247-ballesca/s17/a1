/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userInfo(){
	let name = prompt("What is your name?");
	let age = prompt("How old are you?");
	let address = prompt("Where do you live?");
	alert("Thank you for your input!");
	console.log("Hello, " + name);
	console.log("You are " + age + " years old.");
	console.log("You live in " + address);
}

userInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

	
*/

	function favArtist(){
		console.log("1. Beyonce");
		console.log("2. Kim Molina");
		console.log("3. LadyGaga");
		console.log("4. Eraserheads");
		console.log("5. Mariah Carey");


	}

	favArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function favMovies(){
		console.log("1. You People");
		console.log("Rotten Tomatoes Rating: 45%")
		console.log("2. Infinity Pool");
		console.log("Rotten Tomatoes Rating: 88%")
		console.log("3. Blood");
		console.log("Rotten Tomatoes Rating: 63%")
		console.log("4. The Menu");
		console.log("Rotten Tomatoes Rating: 88%")
		console.log("5. The Order");
		console.log("Rotten Tomatoes Rating: 100%")
	}

	favMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


